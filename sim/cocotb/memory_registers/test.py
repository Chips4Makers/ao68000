import cocotb
from cocotb.triggers import Timer
from cocotb.binary import BinaryValue

@cocotb.coroutine
def clk_cycle(clk, quartperiod, cycles=1):
    for i in range(cycles):
        clk <= 0
        yield quartperiod
        clk <= 1
        yield quartperiod
        yield quartperiod
        clk <= 0
        yield quartperiod


@cocotb.test()
def test01_reset(dut):
    """
    Just test reset
    """

    # Put the clock period to 1000 simulation steps
    quartperiod = Timer(250)

    dut._log.info("Reset")

    dut.reset_n <= 0
    dut.An_write_enable <= 0
    dut.Dn_write_enable <= 0
    dut.micro_pc <= 0

    yield clk_cycle(dut.clock, quartperiod, 3)
    dut.reset_n <= 1
    yield clk_cycle(dut.clock, quartperiod, 1)
    assert dut.usp == 0

    # Write A0
    dut.An_address <= BinaryValue("0000")
    dut.An_input <= BinaryValue("00000000000000000000000000000000")
    dut.An_write_enable <= 1
    # Write byte D0
    dut.Dn_address <= BinaryValue("000")
    dut.Dn_input <= BinaryValue("00000000000000000000000000000000")
    dut.Dn_size <= BinaryValue("001") # 8 Bit
    dut.Dn_write_enable <= 1
    # Read micro op @ address 0
    dut.micro_pc <= BinaryValue("000000000")

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.An_output == BinaryValue("00000000000000000000000000000000")
    assert dut.usp == 0
    assert str(dut.Dn_output)[-8:] == "00000000"
    assert dut.micro_data == BinaryValue("0000000000000000000000000000000000000100000000000000000000000000000000000000000000000000")

    # Write A1 (set supervisor bit)
    dut.An_address <= BinaryValue("1001")
    dut.An_input <= BinaryValue("00000000000000000000000000000010")
    dut.An_write_enable <= 1
    # Write byte D1
    dut.Dn_address <= BinaryValue("001")
    dut.Dn_input <= BinaryValue("00000000000000000000000000000100")
    dut.Dn_size <= BinaryValue("110") # 16 Bit
    dut.Dn_write_enable <= 1
    # Read micro op @ address 1
    dut.micro_pc <= BinaryValue("000000001")

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.An_output == BinaryValue("00000000000000000000000000000010")
    assert dut.usp == 0
    assert str(dut.Dn_output)[-16:] == "0000000000000100"
    assert dut.micro_data == BinaryValue("0111111100000000000000000000000000000000000000000000000000000000000000000000000000000000")

    # Write A7 (=usp)
    dut.An_address <= BinaryValue("0111")
    dut.An_input <= BinaryValue("00000000000000000000000000010000")
    dut.An_write_enable <= 1
    # Write byte D7
    dut.Dn_address <= BinaryValue("111")
    dut.Dn_input <= BinaryValue("00000000000000000000000001000000")
    dut.Dn_size <= BinaryValue("100") # 32 Bit
    dut.Dn_write_enable <= 1
    # Read micro op @ address 5
    dut.micro_pc <= BinaryValue("000000101")

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.An_output == BinaryValue("00000000000000000000000000010000")
    assert dut.usp == BinaryValue("00000000000000000000000000010000")
    assert str(dut.Dn_output) == "00000000000000000000000001000000"
    assert dut.micro_data == BinaryValue("0000000000000000000000000000000000000100000000000000000000000000000000000000000000000000")

    # Write A7 (in supervisor mode)
    dut.An_address <= BinaryValue("1111")
    dut.An_input <= BinaryValue("11000000000000000000000000000011")
    dut.An_write_enable <= 1
    # Write byte D7
    dut.Dn_address <= BinaryValue("111")
    dut.Dn_input <= BinaryValue("00110000000000000000000000001100")
    dut.Dn_size <= BinaryValue("001") # 8 Bit
    dut.Dn_write_enable <= 1
    # Read micro op @ address 21
    dut.micro_pc <= BinaryValue("000010101")

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.An_output == BinaryValue("11000000000000000000000000000011")
    assert dut.usp == BinaryValue("00000000000000000000000000010000")
    assert str(dut.Dn_output)[-8:] == "00001100"
    assert dut.micro_data == BinaryValue("0001111100000000000000000000000000000000000000000000000000000000000000000000000000000000")

    # Read A0
    dut.An_address <= BinaryValue("0000")
    dut.An_input <= BinaryValue("11111111111111111111111111111111")
    dut.An_write_enable <= 0
    # Read byte D0
    dut.Dn_address <= BinaryValue("000")
    dut.Dn_input <= BinaryValue("11111111111111111111111111111111")
    dut.Dn_size <= BinaryValue("001") # 8 Bit
    dut.Dn_write_enable <= 0
    # Read micro op @ address 31
    dut.micro_pc <= BinaryValue("000011111")

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.An_output == BinaryValue("00000000000000000000000000000000")
    assert dut.usp == BinaryValue("00000000000000000000000000010000")
    assert str(dut.Dn_output)[-8:] == "00000000"
    assert dut.micro_data == BinaryValue("0111111100000000000000000000000000000000000000000000000000000000000000000000000000000000")

    # Read A7 (in supervisor mode)
    dut.An_address <= BinaryValue("1111")
    dut.An_input <= BinaryValue("11111111111111111111111111111111")
    dut.An_write_enable <= 0
    # Read byte D7
    dut.Dn_address <= BinaryValue("111")
    dut.Dn_input <= BinaryValue("11111111111111111111111111111111")
    dut.Dn_size <= BinaryValue("100") # 32 Bit
    dut.Dn_write_enable <= 0
    # Read micro op @ address 491
    dut.micro_pc <= BinaryValue("111101011")

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.An_output == BinaryValue("11000000000000000000000000000011")
    assert dut.usp == BinaryValue("00000000000000000000000000010000")
    assert str(dut.Dn_output) == "00000000000000000000000000001100"
    assert dut.micro_data == BinaryValue("0101111111010000000000000000000000000000010000000000000000000000000000000000000000000000")

    dut._log.info("Reset")
